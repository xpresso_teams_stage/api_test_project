#! /bin/bash
## This script is used to run the project. It shuold contain the script which will run the project
##
## DO NOT USE SUDO in the scripts. These scripts are run as sudo user
set -e

# Run the application
export PYTHONPATH=${ROOT_FOLDER}:${PYTHONPATH}
echo "Starting the service"
sleep 10
python3 ${ROOT_FOLDER}/app/main.py $@
